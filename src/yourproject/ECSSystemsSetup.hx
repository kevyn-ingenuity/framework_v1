package yourproject;

import edge.*;

import ecs.kit.render.*;
import ecs.id.*;
import ecs.setup.World;

import ecs.input.pointer.*;
import ecs.input.keyboard.*;
import ecs.save_load.*;
import ecs.destroy.*;
import ecs.scene.*;

/**
 *  SETUP THE REQUIRED SYSTEMS FOR YOUR CURRENT PROJECT. THIS IS ENGINE INDEPENDENT
 */
class ECSSystemsSetup
{
    public static function AddSystems(world : World)
    {
        IDSystems.Setup(world);
        //add features here
        InputPointerSystems.Setup(world);
        InputKeyboardSystems.Setup(world);
        SaveLoadSystems.Setup(world);
        DestroySystems.Setup(world);
        SceneSystem.Setup(world);
    }
}