//
// 2DKit - Rapid game development
// http://2dkit.com

package yourproject;

import kit.Component;
import kit.Entity;
import kit.System;

import ez.EzApp;

import yourproject.ECSSystemsSetup;
import ecs.setup.ECSSetup;

import ecs.setup.World;

/** The main component class. */
class YourProject extends Component
{
    /** Called when the component is started. */
    override public function onStart ()
    {
        var ez = new EzApp("Home");

        ez.loaded.connect(function()
        {
            var setup = new ECSSetup();
            setup.onSetupComplete.connect(function(world)
            {
                ECSSystemsSetup.AddSystems(world);
            });
            owner.add(setup);
        });

        owner.add(ez);
    }
}
