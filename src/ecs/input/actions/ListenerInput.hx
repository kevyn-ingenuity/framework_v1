package ecs.input.actions;

import edge.*;

import kit.System;
import kit.creator.CreatorAction;
import kit.display.Sprite;

import kit.Entity;

import ecs.object.ECSEntity;
import ecs.components.*;
import ecs.input.InputType;
import ecs.input.pointer.PointerInputData;

class ListenerInput extends CreatorAction
{
    public var event : String = "";
    public var global : Bool = false;
    public var inputType : InputType = InputType.None;

    override public function onRun(target : Entity)
    {
        var entity : edge.Entity = target.get(ECSEntity).instance;

        //entity.add(new ListenToPointerInput(event, global));
        switch inputType
        {
            case InputType.Pointer:
                AddPointerEvent(entity, event, global);
            case InputType.Keyboard:
                AddKeyEvent(entity, event);
            default: trace("no input type chosen");
        }

        entity.add(new KitSprite(target.get(Sprite)));
    }

    function AddPointerEvent(entity : edge.Entity, event : String, global : Bool)
    {
        if (entity.existsType(ListenToPointerInput))
        {
            entity.get(ListenToPointerInput).eventList.push(new PointerInputData(event, global));
        }
        else
        {
            var newData : Array<PointerInputData> = new Array();
            newData.push(new PointerInputData(event, global));
            entity.add(new ListenToPointerInput(newData));
        }
    }

    function AddKeyEvent(entity : edge.Entity, event : String)
    {
        if (entity.existsType(ListenToKeyInput))
        {
            entity.get(ListenToKeyInput).eventList.push(event);
        }
        else
        {
            var newData : Array<String> = new Array();
            newData.push((event));
            entity.add(new ListenToKeyInput(newData));
        }
    }
}