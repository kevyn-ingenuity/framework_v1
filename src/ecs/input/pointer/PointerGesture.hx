package ecs.input.pointer;

enum PointerGesture
{
    None;
    Down;
    Move;
    Up;
}