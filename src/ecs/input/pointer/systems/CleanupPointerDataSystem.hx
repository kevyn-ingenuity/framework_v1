package ecs.input.pointer.systems;

import edge.*;

import ecs.components.*;

class CleanupPointerDataSystem implements ISystem
{
    var entity : Entity;

    public function update(data : PointerData)
    {
        entity.remove(data);
    }
}