package ecs.input.pointer;

class PointerInputData
{
    public var event : String;
    public var global : Bool;

    public function new (event : String, global : Bool)
    {
        this.event = event;
        this.global = global;
    }
}