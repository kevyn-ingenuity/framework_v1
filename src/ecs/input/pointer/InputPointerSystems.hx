package ecs.input.pointer;

import ecs.input.pointer.systems.*;

import edge.*;

import ecs.setup.World;

class InputPointerSystems
{
    public static function Setup (world : World)
    {
        world.input.add(new EmitPointerDataDownSystem());
        world.input.add(new EmitPointerDataMoveSystem());
        world.input.add(new EmitPointerDataUpSystem());

        world.cleanup.add(new CleanupPointerDataSystem());
    }
}