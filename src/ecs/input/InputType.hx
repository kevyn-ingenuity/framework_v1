package ecs.input;

enum InputType
{
    None;
    Pointer;
    Keyboard;
    Motion;
    Gamepad;
}