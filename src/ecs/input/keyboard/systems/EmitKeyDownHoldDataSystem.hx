package ecs.input.keyboard.systems;

import ecs.components.*;

import kit.System;
import kit.input.KeyboardEvent;

using hxLINQ.LINQ;

import edge.*;

class EmitKeyDownHoldDataSystem implements ISystem
{
    var listeners : edge.View<{ ltr : ListenToKeyInput }>;

    public function update (keyEvent : KeyInputDownHold) 
    {
        if (System.keyboard.isDown(keyEvent.key))
        {
            //filter entities
            var validLtrs : Array<edge.Entity> = listeners.linq()
                .selectMany(function(ltr, index)
                {
                    return ltr.data.ltr.eventList.linq()
                        .where(function(event, index)
                        {
                            return event == keyEvent.event;
                        })
                        .select(function(ptrData, index) return ltr.entity).toArray();
                }).toArray();

            //add data to entities that passed the filtering
            for(entity in validLtrs)
            {
                if(entity.existsType(KeyData))
                {
                    var currPtrData : Map<String, KeyboardEvent> = entity.get(KeyData).data;
                    if (currPtrData.exists(keyEvent.event))
                    {
                        currPtrData[keyEvent.event] = null;
                    }
                    else
                    {
                        currPtrData.set(keyEvent.event, null);
                    }
                }
                else
                {
                    var newData : Map<String, KeyboardEvent> = new Map();
                    newData.set(keyEvent.event, null);
                    entity.add(new KeyData(newData));
                }
            }
        }
    }

}