package ecs.input.keyboard;

enum KeyGesture
{
    None;
    Down;
    DownHold;
    Up;
}