package ecs.kit.action;

import kit.creator.CreatorAction;
import kit.creator.CreatorObject;
import kit.debug.FpsDisplay;
import kit.display.TextSprite;
import kit.display.Font;
import kit.creator.ui.BitmapText;

import kit.Entity;
import kit.System;
import kit.util.Assert;

import ecs.object.ECSEntity;

import edge.*;

/**
 *  TRACE THE ECS ENTITY STACK
 */
class DisplayFPS extends CreatorAction
{
    override public function onRun(target : Entity)
    {
        Assert.that(target.get(BitmapText) != null, "Must attach to bitmap text object");
        var textSprite = target.get(BitmapText).getTextSprite();
        var fpsMeterEntity = new Entity().add(textSprite).add(new FpsDisplay());
        System.root.addChild(fpsMeterEntity);
    }
}