package ecs.kit.action;

import kit.creator.CreatorAction;
import kit.creator.SceneSprite;

import kit.Entity;
import kit.creator.SceneInfo;
import kit.Assets;

import edge.*;

import ecs.object.ECSEntity;

/**
 *  LOADS SYMBOLS FROM THE ACTIVE SCENE. IF THERE IS LOADING ERROR, ADD THE ASSET PACK THE SYMBOL BELONGS TO IN THE CURRENT SCENE PROPERTIES
 */
class LoadSymbol extends CreatorAction
{
    public var name : String;
    public var layer : String;

    override public function onRun(target : Entity)
    {
        var entity : edge.Entity = target.get(ECSEntity).instance;
        var assets = target.getFromParents(Assets);

        var symbol : SceneInfo = assets.getSceneSymbol(name, true);
        var sprite : SceneSprite = symbol.createSprite();
        var layer = target.getFromParents(SceneSprite).getLayer(layer);
        var symbolEntity = new Entity().add(sprite).add(symbol.createScript());
        layer.addChild(symbolEntity);

        sprite.owner.yieldForStart();
    }
}