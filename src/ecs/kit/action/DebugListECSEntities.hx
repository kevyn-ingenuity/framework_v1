package ecs.kit.action;

import kit.creator.CreatorAction;
import kit.creator.CreatorObject;

import kit.Entity;
import kit.System;

import ecs.object.ECSEntity;

import edge.*;

/**
 *  TRACE THE ECS ENTITY STACK
 */
class DebugListECSEntities extends CreatorAction
{
    override public function onRun(target : Entity)
    {
        var entity = target.get(ECSEntity);
        trace(entity.engine.entities());
    }
}