package ecs.kit.action;

import edge.*;

import kit.creator.CreatorAction;
import kit.Entity;
import kit.System;
import kit.creator.PropertyBag;
import kit.creator.CreatorScript;
import kit.creator.GroupAction;
import kit.util.Promise;

import ez.property.PropertyType;
import ez.property.PropertyAction;

import ecs.object.ECSEntity;

class BindToField extends PropertyAction
{
    public var actionGroup : String;
    public var targetAction : String;
    public var targetField : String;
    public var type :PropertyType = Number;
    public var initialValue : String;
    public var runActionGroupOnValueChange : Bool;

    var bag : PropertyBag;
    var actions : Array<CreatorAction>;

    override private function getValue (target :Entity, currentValue :Dynamic) :Dynamic
    {
        bag = target.getFromParents(PropertyBag);
        bag.changed.connect(function(fieldName, newValue){
            if (fieldName != property){ return; }
            onUpdateValue(newValue);
            if (runActionGroupOnValueChange)
            {
                script.owner.emitMessage(actionGroup, target);
            }
        });
        Setup();

        return PropertyAction.convert(type, initialValue);
    }

    function Setup()
    {
        //get target group
        var groups : Array<Group> = script.groups
            .filter(function(group){
                return group.info.name == actionGroup; 
            });
        
        for (group in groups)
        {
            //get target actions
            actions = group.actions.filter(function(action)
            {
                var result = Type.getClassName(Type.getClass(action));
                return result != null ? result.indexOf(targetAction) >= 0 : false;
            });
        }
    }

    function onUpdateValue(newValue : Dynamic)
    {
        //apply to actions the new initialValue
        for (action in actions)
        {
            Reflect.setField(action, targetField, newValue);
        }
    }
}