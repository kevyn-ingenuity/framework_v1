package ecs.destroy;

import ecs.save_load.*;

import edge.*;

import ecs.setup.World;

class DestroySystems
{
    public static function Setup (world : World)
    {
        world.cleanup.add(new DestroyDelaySystem());
    }
}