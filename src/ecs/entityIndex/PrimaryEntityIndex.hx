package ecs.entityIndex;

@:generic
class PrimaryEntityIndex<TEntity : {}, TKey : Int>
{
    var _index : Map<TKey, TEntity>;

    public function new ()
    {
        _index = new Map<TKey, TEntity>();
    }

    public function Add(id : TKey, entity : TEntity) : Bool
    {
        if (_index.exists(id))
        {
            return false;
        }
        else
        {
            _index.set(id, entity);
            return true;
        }
    }

    public function Get(id : TKey) : TEntity
    {
        return _index.get(id);
    }

    public function Remove(id : TKey) : Bool
    {
        return _index.remove(id);
    }

    public function Clear() : Bool
    {
        for (key in _index.keys())
        {
            _index.remove(key);
        }

        return true;
    }


}