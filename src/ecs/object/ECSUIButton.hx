package ecs.object;

import kit.creator.ui.Button;

import ecs.object.ECSEntity;

/**
 *  THE TYPE THE OBJECTS SHOULD HAVE IN THE CREATOR SO THEY CAN BE READ BY THE ECS SYSTEM
 */
class ECSUIButton extends Button
{
    public var initComponents : String;

    public var entity(default, null) : ECSEntity;

    override public function onStart()
    {
        entity = new ECSEntity(owner);

        if (initComponents != null)
        {
            owner.emitMessageToParents(initComponents, owner);
        }
    }
}