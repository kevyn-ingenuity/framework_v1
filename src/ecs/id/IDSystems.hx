package ecs.id;

import ecs.id.*;

import edge.*;

import ecs.setup.World;

class IDSystems
{
    public static function Setup (world : World)
    {
        world.input.add(new IDIndexingAndFactorySystem());
        world.input.add(new ExtensionIDIndexingSystem());
    }
}