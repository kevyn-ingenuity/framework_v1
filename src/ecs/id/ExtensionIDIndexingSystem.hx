package ecs.id;

import ecs.components.*;
import ecs.id.IDGenerator;
import ecs.id.ExtensionIDHelper;

import edge.*;

/**Handles adding and removing of CptID
 *  Also, handles Primary Entity Indexing
*/
class ExtensionIDIndexingSystem implements ISystem
{
    
    public function updateAdded (entity: Entity, data : {id : IdExtension})
    {
        ExtensionIDHelper.Add(data.id.number, entity);
    }

    public function update(id : IdExtension){}

    public function updateRemoved (entity: Entity, data : {id : IdExtension})
    {
        ExtensionIDHelper.Remove(data.id.number);
    }

}