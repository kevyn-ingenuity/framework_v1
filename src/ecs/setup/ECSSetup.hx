package ecs.setup;

import kit.Component;
import kit.System;
import kit.util.Signal1;

import edge.*;

import ecs.setup.World;

/**
 *  INJECTS ECS SYSTEM WITH 2DKIT
 */
class ECSSetup extends Component
{
    public var world(default, null) : World;

    public var onSetupComplete : Signal1<World> = new Signal1();

    var updateSignal : Signal1<Float> = new Signal1();

    override public function onStart()
    {
        //setup ecs world
        world = new World(16, callbackUpdate);

        onSetupComplete.emit(world);

        //add 2dkit dependencies as entities
        //root systems (director, scenesprites etc.)
        //world.engine.create([new CptKitRoot(System.root)]);

        world.start();
    }

    override public function onUpdate(dt : Float)
    {
        if (updateSignal.hasListeners())
        {
            updateSignal.emit(dt);
        }
    }

    override public function onStop()
    {
        world.stop();
    }

    function callbackUpdate(toUpdate : Float -> Void)
    {
        var connection = updateSignal.connect(
        function (dt : Float)
        {
            toUpdate(dt);
        });

        return connection.dispose;
    }
}