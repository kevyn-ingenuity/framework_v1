package ecs.setup;

import edge.*;

import ecs.kit.render.*;
import ecs.id.*;
import ecs.input.pointer.pointerpointer.pointer.*;
import ecs.setup.World;

/**
 *  SETUP THE REQUIRED SYSTEMS FOR YOUR CURRENT PROJECT. THIS IS ENGINE INDEPENDENT
 */
class ECSSystemsSetup
{
    public static function AddSystems(world : World)
    {
        IDSystems.Setup(world);
        //add features here
    }
}