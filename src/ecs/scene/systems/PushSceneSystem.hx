package ecs.scene.systems;

import edge.*;

import ecs.scene.components.PushSceneComponents;

import kit.System;

import kit.scene.Director;

import ez.EzScene;

class PushSceneSystem implements ISystem {
    public function update (scene : PushSceneComponents) {
        if(scene.scene != null) {
            // trace(scene.scene);
            System.root.get(Director).pushScene(new EzScene(scene.scene));
        } else {
            try {
                throw "No next scene";
            } catch(msg : String) {
                trace("Error: "+msg);
            }
        }
    }
}