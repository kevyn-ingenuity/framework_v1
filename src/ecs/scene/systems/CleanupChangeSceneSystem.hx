package ecs.scene.systems;

import edge.*;

import ecs.scene.components.*;

class CleanupChangeSceneSystem implements ISystem {
    var entity : Entity;

    public function update(data : ChangeSceneComponents) {
        // trace(data);
        entity.remove(data);
    }
}