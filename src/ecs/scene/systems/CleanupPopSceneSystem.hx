package ecs.scene.systems;

import edge.*;

import ecs.scene.components.*;

class CleanupPopSceneSystem implements ISystem {
    var entity : Entity;

    public function update(data : PopSceneComponents) {
        // trace(data);
        entity.remove(data);
    }
}