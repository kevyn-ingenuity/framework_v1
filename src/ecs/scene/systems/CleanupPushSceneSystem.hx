package ecs.scene.systems;

import edge.*;

import ecs.scene.components.*;

class CleanupPushSceneSystem implements ISystem {
    var entity : Entity;

    public function update(data : PushSceneComponents) {
        // trace(data);
        entity.remove(data);
    }
}