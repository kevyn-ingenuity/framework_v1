package ecs.scene.systems;

import edge.*;

import ecs.scene.components.LoadingSceneComponents;

import ez.EzScene;

import kit.System;

import kit.scene.Director;

class LoadingSceneSystem implements ISystem {
    
    public function update(loadingScene : LoadingSceneComponents) {
        //Action for the loading scene system here
        if(loadingScene.flag) {
            if(loadingScene.scene != null) {
                System.root.get(Director).pushScene(new EzScene(loadingScene.scene));
            } else {
                try {
                    throw "No loading scene";
                } catch(msg : String) {
                    trace("Error: "+msg);
                }
            }
        } else {
            if(loadingScene.loadedScene != null) {
                System.root.get(Director).unwindToScene(new EzScene(loadingScene.loadedScene));
            } else {
                try {
                    throw "No loaded/next scene";
                } catch(msg : String) {
                    trace("Error: "+msg);
                }
            }
        }
    }
}