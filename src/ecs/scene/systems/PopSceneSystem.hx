package ecs.scene.systems;

import edge.*;

import ecs.scene.components.PopSceneComponents;

import kit.System;

import kit.scene.Director;

class PopSceneSystem implements ISystem {
    public function update (scene : PopSceneComponents) {
        System.root.get(Director).popScene();
    }
}