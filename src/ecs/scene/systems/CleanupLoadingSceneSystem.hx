package ecs.scene.systems;

import edge.*;

import ecs.scene.components.*;

class CleanupLoadingSceneSystem implements ISystem {
    var entity : Entity;

    public function update(data : LoadingSceneComponents) {
        // trace(data);
        entity.remove(data);
    }
}