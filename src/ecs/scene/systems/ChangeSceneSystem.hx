package ecs.scene.systems;

import ecs.object.ECSEntity;

import edge.*;

import ecs.scene.components.ChangeSceneComponents;
import ecs.scene.components.LoadingSceneComponents;

import kit.Assets;

class ChangeSceneSystem implements ISystem {
    var entity : Entity;

    public function update (scene : ChangeSceneComponents) {
        if(scene.scene != null) {
            if(scene.assetFolder != null) {
                new Assets().load(scene.assetFolder).then(function (_) {
                    trace("Fully loaded");

                    entity.add(new LoadingSceneComponents(100.00, scene.loadingScene, scene.scene, false));
                }).progress(function (progress) {
                    trace("Percent loaded: "+(progress*100));

                    entity.add(new LoadingSceneComponents(progress, scene.loadingScene, scene.scene, true));
                });
            } else {
                try {
                    throw "No assetFolder";
                } catch(msg : String) {
                    trace("Error: "+msg);
                }
            }
        } else {
            try {
                throw "No next scene";
            } catch(msg : String) {
                trace("Error: "+msg);
            }
        }
    }
}