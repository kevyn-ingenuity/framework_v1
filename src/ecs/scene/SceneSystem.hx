package ecs.scene;

import ecs.scene.systems.*;

import edge.*;

import ecs.setup.World;

class SceneSystem {
    public static function Setup (world : World) {
        world.frame.add(new ChangeSceneSystem());
        world.frame.add(new PopSceneSystem());
        world.frame.add(new PushSceneSystem());
        world.frame.add(new LoadingSceneSystem());
        world.frame.add(new CleanupChangeSceneSystem());
        world.frame.add(new CleanupPopSceneSystem());
        world.frame.add(new CleanupPushSceneSystem());
        world.frame.add(new CleanupLoadingSceneSystem());
    }
}