package ecs.scene.components;

import ecs.object.ECSEntity;

import kit.Entity;

import kit.creator.CreatorAction;

import ecs.scene.components.PopSceneComponents;

class ListenPopComponent extends CreatorAction {
    override public function onRun(target : Entity) {
        var entity : edge.Entity = target.get(ECSEntity).instance;
        entity.add(new PopSceneComponents());
        // trace("entity: "+entity);
    }
}