package ecs.scene.components;

import ecs.object.ECSEntity;

import kit.Entity;

import kit.creator.CreatorAction;

import ecs.scene.components.ChangeSceneComponents;

class ListenChangeComponent extends CreatorAction {
    public var scene : String;
    public var loadingScene : String;
    public var assetFolder : String;

    override public function onRun(target : Entity) {
        var entity : edge.Entity = target.get(ECSEntity).instance;
        entity.add(new ChangeSceneComponents(scene, loadingScene, assetFolder));
        // trace(target.get(ECSEntity));
    }
}