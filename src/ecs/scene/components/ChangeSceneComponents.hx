package ecs.scene.components;

import edge.IComponent;

class ChangeSceneComponents implements IComponent {
    var scene : String;
    var loadingScene : String;
    var assetFolder : String;
}