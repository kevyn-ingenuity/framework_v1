package ecs.scene.components;

import ecs.object.ECSEntity;

import kit.Entity;

import kit.creator.CreatorAction;

import ecs.scene.components.PushSceneComponents;

class ListenPushComponent extends CreatorAction {
    public var scene : String;

    override public function onRun(target : Entity) {
        var entity : edge.Entity = target.get(ECSEntity).instance;
        entity.add(new PushSceneComponents(scene));
        // trace("scene1: "+scene);
    }
}