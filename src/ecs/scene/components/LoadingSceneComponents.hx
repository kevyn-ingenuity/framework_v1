package ecs.scene.components;

import edge.*;

class LoadingSceneComponents implements IComponent {
    var progress : Float;
    var scene : String;
    var loadedScene : String;
    var flag : Bool;
}