package ecs.save_load;

import edge.*;

import kit.System;

import ecs.components.*;

class CleanupSaveLoadSystem implements ISystem
{
    var save : edge.View<{ cpt : Save }>;
    var load : edge.View<{ cpt : Load }>;

    public function update ()
    {
        for (view in save)
        {
            view.entity.remove(view.data.cpt);
        }

        for (view in load)
        {
            view.entity.remove(view.data.cpt);
        }
    }
}