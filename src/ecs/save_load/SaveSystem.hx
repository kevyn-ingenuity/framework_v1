package ecs.save_load;

import edge.*;

import kit.System;
import kit.creator.PropertyBag;

import kit.util.Promise;

import ecs.components.*;

class SaveSystem implements ISystem
{
    var entity : Entity;

    public function update (save : Save)
    {
        var local = System.localStorage;
        if (local == null){ trace("Local storage not supported by target platform. Data will be lost."); }

        //copy and remove id and save components
        var saveEntity = entity.engine.create();
        for (cpt in entity.components())
        {
            saveEntity.add(cpt);
        }
        saveEntity.removeType(Id);
        saveEntity.removeType(Save);

        var progress = local.set(save.key, saveEntity.components());

        entity.add(new SaveProgress(progress));
        saveEntity.add(new Destroy(progress));
    }
}