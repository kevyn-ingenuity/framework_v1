package ecs.save_load;

import ecs.save_load.*;

import edge.*;

import ecs.setup.World;

class SaveLoadSystems
{
    public static function Setup (world : World)
    {
        world.input.add(new SaveSystem());
        world.input.add(new LoadSystem());

        world.cleanup.add(new CleanupSaveLoadSystem());
        world.cleanup.add(new CleanupSaveProgressSystem());
        world.cleanup.add(new CleanupLoadProgressSystem());
    }
}