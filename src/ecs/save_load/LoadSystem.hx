package ecs.save_load;

import edge.*;

import kit.System;
import kit.creator.PropertyBag;

import kit.util.Promise;

import ecs.id.IDGenerator;

import ecs.components.*;

class LoadSystem implements ISystem
{
    var entity : Entity;

    public function update (load : Load)
    {
        var local = System.localStorage;
        if (local == null){ trace("Local storage not supported by target platform. Data will be lost."); }

        var progress = local.get(load.key, null);
        
        entity.add(new LoadProgress(progress));
        
        progress.then(function(result : Iterator<{}>){
            for (data in result)
            {
                entity.add(data);
            }
        });
    }
}